/* **********************************************************
 * Copyright (c) 2014-2018 Google, Inc.  All rights reserved.
 * Copyright (c) 2008 VMware, Inc.  All rights reserved.
 * **********************************************************/

/*
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * * Neither the name of VMware, Inc. nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL VMWARE, INC. OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

/* Code Manipulation API Sample:
 * exec_stats.cpp
 *
 * Reports the dynamic execution count of all basic blocks.
 * Illustrates how to perform performant inline increments with analysis
 * on whether flags need to be preserved.
 */

#include "string.h" /* for memset */
#include "dr_api.h"
#include "dr_ir_utils.h"
#include "drmgr.h"
#include "droption.h"
#include "drreg.h"
#include "drx.h"


#ifdef WINDOWS
#    define DISPLAY_STRING(msg) dr_messagebox(msg)
#else
#    define DISPLAY_STRING(msg) dr_printf("%s\n", msg);
#endif

#define BUFFER_SIZE_BYTES(buf) sizeof(buf)
#define BUFFER_SIZE_ELEMENTS(buf) (BUFFER_SIZE_BYTES(buf) / sizeof((buf)[0]))
#define NULL_TERMINATE(buf) (buf)[(sizeof((buf)) / sizeof((buf)[0])) - 1] = '\0'
#define CHAR3_TO_INT(ptr) ((*(int*)(ptr) & 0xffffff))
#define MAX(x, y) ((x) >= (y)) ? (x) : (y)

static droption_t<bool> all_modules(
    DROPTION_SCOPE_CLIENT, "all", false,
    "Count all instructions (app +lib)",
    "Count all instructions in the application and instructions in "
    "shared libraries.");

static droption_t<bool> quiet_mode(
    DROPTION_SCOPE_CLIENT, "quiet", false,
    "Quiet (disable stdout logging)",
    "logging to stdout is disabled (still logs to log_dir)");

static droption_t<std::string> log_dir(
    DROPTION_SCOPE_CLIENT, "log_dir", "/tmp",
    "logs are written to log_dir",
    "logging directory (default = /tmp)");

/* Application module */
static app_pc exe_start;

/* Globals */
static client_id_t client_id;

/* global stats struct */
typedef struct _client_stats {
    process_id_t pid;
    bool exited;
    uint num_instrs;
    uint num_flops;
    uint num_syscalls;
    uint num_blocks;
    uint num_preds;
    uint num_cmps[16];
    uint num_fcmps[16];
} client_stats_t;

typedef struct _per_bb_data_t {
    uint num_instrs;
    uint num_flops;
    uint num_syscalls;
    uint num_preds;
    uint num_cmps[16];
    uint num_fcmps[16];
} per_bb_data_t;

/* Global stats */
static client_stats_t stats = { 0 };

static dr_emit_flags_t
event_bb_analysis(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                  bool translating, void **user_data);

static dr_emit_flags_t
event_app_instruction(void *drcontext, void *tag, instrlist_t *bb, instr_t *instr,
                      bool for_trace, bool translating, void *user_data);

static void
write_results_to_logfile(char *msg) {
    char buf[MAXIMUM_PATH];
    const char *dirpath = log_dir.get_value().c_str();
    if ((!dr_directory_exists(dirpath)) && (!dr_create_dir(dirpath))) {
        dr_printf("Error: specified logging directory cannot be created! (%s)\n", dirpath);
        return;
    }

    file_t log_fd = drx_open_unique_appid_file(dirpath, 
                                               dr_get_process_id(), "dr_exec_stats", "log", 0,
                                               buf, BUFFER_SIZE_ELEMENTS(buf));
    dr_printf(" --- wrote results to %s (%u bytes)\n", buf, dr_fprintf(log_fd, msg));
    dr_close_file(log_fd);
}

static void
event_exit(void)
{
    char msg[2048];
    uint len, i, n_cmps = 0, n_fcmps = 0;

    for (i=1; i<9; i++) {
	    n_cmps += stats.num_cmps[i];
	    n_fcmps += stats.num_fcmps[i];
    }

    len = dr_snprintf(msg, sizeof(msg) / sizeof(msg[0]),
                      "{\n \"exec_stats\": {\n"
		      "    \"instructions\": %u,\n"
		      "    \"fp_instructions\": %u,\n"
                      "    \"basic_blocks\": %u,\n"
		      "    \"syscalls\": %u,\n"
		      "    \"pred_instrs\": %u,\n"
		      "    \"cmp_instrs\": %u,\n"
		      "    \"fcmp_instrs\": %u\n"
		      "    },\n",
                      stats.num_instrs, stats.num_flops, stats.num_blocks,  
		      stats.num_syscalls, stats.num_preds, 
		      n_cmps, n_fcmps);
    len += dr_snprintf(msg+len, sizeof(msg)-len, 
                       " \"cmp_by_size\": [%u", stats.num_cmps[0]);
    for (i = 1; i<11; i++) {
        len += dr_snprintf(msg+len, sizeof(msg)-len, ", %u", stats.num_cmps[i]);
    }

    len += dr_snprintf(msg+len, sizeof(msg)-len, 
                       "],\n \"fcmp_by_size\": [%u", stats.num_fcmps[0]);
    for (i = 1; i<11; i++) {
        len += dr_snprintf(msg+len, sizeof(msg)-len, ", %u", stats.num_fcmps[i]);
    }
    len += dr_snprintf(msg+len, sizeof(msg)-len, "]\n}");
             
    DR_ASSERT(len > 0);
    NULL_TERMINATE(msg);
#ifdef SHOW_RESULTS
    if (!quiet_mode.get_value())
	    DISPLAY_STRING(msg);
#endif /* SHOW_RESULTS */
    write_results_to_logfile(msg);
    drx_exit();
    if (!drmgr_unregister_bb_instrumentation_event(event_bb_analysis))
	    DR_ASSERT(false);
    drreg_exit();
    drmgr_exit();
}

bool
opc_is_cmp(instr_t *instr)
{
        int opc = instr_get_opcode(instr);
	return ((opc == OP_cmp) || 
		(opc >= OP_pcmpgtb && opc <= OP_pcmpgtd) ||
		(opc >= OP_pcmpeqb && opc <= OP_pcmpeqd) ||
	        (opc == OP_cmpxchg) || 
		(opc == OP_cmpxchg8b) ||
		(opc >= OP_ucomiss && opc <= OP_comisd) ||
		(opc == OP_fcom) || (opc == OP_fcomp) ||
		(opc == OP_ficom) || (opc == OP_ficomp) ||
		(opc >= OP_cmpps && opc <= OP_cmpsd) ||
		(opc >= OP_cmps && opc <= OP_repne_cmps));
}

bool
instr_is_cmp(instr_t *instr) 
{
        int opc = instr_get_opcode(instr);
	const char *p = decode_opcode_name(opc);
	int cmp= *(int*)"cmp";
	int com = *(int*)"com";
	return ((cmp == CHAR3_TO_INT(p)) ||
		(cmp == CHAR3_TO_INT(p+1)) ||
		(cmp == CHAR3_TO_INT(p+2)) ||
		(com == CHAR3_TO_INT(p)) ||
		(com == CHAR3_TO_INT(p+1)) ||
		(com == CHAR3_TO_INT(p+2)) );
}

static dr_emit_flags_t
event_bb_analysis(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
                  bool translating, void **user_data)
{
    instr_t *instr;
    uint num_instrs = 0;
    uint num_flops = 0;
    uint num_syscalls = 0;
    uint num_preds = 0;
    uint num_cmps = 0;
    dr_fp_type_t fp_type;

    /* Only count in app BBs */
    if (!all_modules.get_value()) {
        module_data_t *mod = dr_lookup_module(dr_fragment_app_pc(tag));
        if (mod != NULL) {
            bool from_exe = (mod->start == exe_start);
            dr_free_module_data(mod);
            if (!from_exe) {
                *user_data = NULL;
                return DR_EMIT_DEFAULT;
            }
        }
    }

    per_bb_data_t *per_bb = (per_bb_data_t*)dr_thread_alloc(drcontext, sizeof(*per_bb));
    memset(per_bb, 0, sizeof(*per_bb));

#ifdef VERBOSE
    dr_printf("analysis in dynamorio_basic_block(tag=" PFX ")\n", tag);
#    ifdef VERBOSE_VERBOSE
    instrlist_disassemble(drcontext, tag, bb, STDOUT);
#    endif
#endif

    /* Count instructions. If an emulation client is running with this client,
     * we want to count all the original native instructions and the emulated
     * instruction but NOT the introduced native instructions used for emulation.
     */
    bool is_emulation = false;
    for (instr = instrlist_first(bb), num_instrs = 0; instr != NULL;
         instr = instr_get_next(instr)) {

        if (drmgr_is_emulation_start(instr)) {
            /* Each emulated instruction is replaced by a series of native
             * instructions delimited by labels indicating when the emulation
             * sequence begins and ends. It is the responsibility of the
             * emulation client to place the start/stop labels correctly.
             */
            num_instrs++;
            is_emulation = true;
            /* Data about the emulated instruction can be extracted from the
             * start label using the accessor function:
             * drmgr_get_emulated_instr_data()
             */
            continue;
        }
        if (drmgr_is_emulation_end(instr)) {
            is_emulation = false;
            continue;
        }
        if (is_emulation)
            continue;
        if (!instr_is_app(instr))
            continue;
        num_instrs++;

        if (instr_is_floating_ex(instr, &fp_type) && 
           /* We exclude loads and stores (and reg-reg moves) and state preservation */
           (fp_type == DR_FP_CONVERT || fp_type == DR_FP_MATH)) {
               num_flops++;
        }
        if (instr_is_syscall(instr)) {
            num_syscalls++;
        }
        if (instr_get_predicate(instr) != 0) {
            num_preds++;
#if defined(VERBOSE) && defined(VERBOSE_VERBOSE)
            dr_printf("pred(%u) :", instr_get_predicate(instr));
            instr_disassemble(drcontext, instr, STDOUT);
            dr_printf("\n");
#endif
        }
        int opnd_size;
        if (instr_is_cmp(instr)) {
            opnd_size = MAX(opnd_get_size(instr_get_src(instr, 0)), 
                            opnd_get_size(instr_get_src(instr, 1)));

            if (instr_is_floating(instr)) {
                per_bb->num_fcmps[0] |= (1 << opnd_size); 
                per_bb->num_fcmps[opnd_size] += 1;
#if defined(VERBOSE) && defined(VERBOSE_VERBOSE)
		dr_printf("FCMP(%u) :", opnd_size);
		dr_printf("OP(%d) ", instr_get_opcode(instr));
		instr_disassemble(drcontext, instr, STDOUT);
		dr_printf("\n");
#endif
            } else {
                per_bb->num_cmps[0] |= (1 << opnd_size); 
                per_bb->num_cmps[opnd_size] += 1;
#if defined(VERBOSE) && defined(VERBOSE_VERBOSE)
		dr_printf("_CMP(%u) :", opnd_size);
		dr_printf("OP(%d) ", instr_get_opcode(instr));
		instr_disassemble(drcontext, instr, STDOUT);
		dr_printf("\n");
#endif
            }

        }

    }

    per_bb->num_instrs = num_instrs;
    per_bb->num_flops = num_flops;
    per_bb->num_syscalls = num_syscalls;
    per_bb->num_preds = num_preds;

    *(per_bb_data_t **)user_data = per_bb;

#if defined(VERBOSE) && defined(VERBOSE_VERBOSE)
    dr_printf("Finished counting for dynamorio_basic_block(tag=" PFX ")\n", tag);
    instrlist_disassemble(drcontext, tag, bb, STDOUT);
#endif
    return DR_EMIT_DEFAULT;
}

static dr_emit_flags_t
event_app_instruction(void *drcontext, void *tag, instrlist_t *bb, instr_t *instr,
                      bool for_trace, bool translating, void *user_data)
{
    per_bb_data_t *per_bb = (per_bb_data_t*)user_data;
    uint n0;
    dr_spill_slot_t spill_slot_max = static_cast<dr_spill_slot_t>(static_cast<int>(SPILL_SLOT_MAX) + 1);

    /* By default drmgr enables auto-predication, which predicates all instructions with
     * the predicate of the current instruction on ARM.
     * We disable it here because we want to unconditionally execute the following
     * instrumentation.
     */
    drmgr_disable_auto_predication(drcontext, bb);
    if (!drmgr_is_first_instr(drcontext, instr))
        return DR_EMIT_DEFAULT;

    if (user_data == NULL)
        return DR_EMIT_DEFAULT;


    /* DRX_COUNTER_LOCK shoud work (not on ARM) */
    uint flags = DRX_COUNTER_LOCK;

    drx_insert_counter_update(drcontext, bb, instr,
                              /* We're using drmgr, so these slots
                               * here won't be used: drreg's slots will be.
                               */
                              spill_slot_max,
                              IF_AARCHXX_(spill_slot_max) &stats.num_blocks, 1, flags);

    drx_insert_counter_update(drcontext, bb, instr, spill_slot_max,
                              &stats.num_instrs, per_bb->num_instrs, flags);
   
    if (per_bb->num_flops != 0) {
        drx_insert_counter_update(drcontext, bb, instr, spill_slot_max,
                                  &stats.num_flops, per_bb->num_flops, flags);
    }
    if (per_bb->num_syscalls != 0) {
        drx_insert_counter_update(drcontext, bb, instr, spill_slot_max,
                                  &stats.num_syscalls, per_bb->num_syscalls, flags);
    }
    if (per_bb->num_preds != 0) {
        drx_insert_counter_update(drcontext, bb, instr, spill_slot_max,
                                  &stats.num_preds, per_bb->num_preds, flags);
    }
    if (per_bb->num_cmps[0] != 0) {
        n0 = per_bb->num_cmps[0];
        for (int i=1; i < 11; i++ ) {
            if ((n0 >> i) & 1) {
                drx_insert_counter_update(drcontext, bb, instr, spill_slot_max,
                                          &stats.num_cmps[i], per_bb->num_cmps[i], flags);
	    }
	}
    }
    if (per_bb->num_fcmps[0] != 0) {
        n0 = per_bb->num_fcmps[0];
        for (int i=1; i < 11; i++ ) {
            if ((n0 >> i) & 1) {
                drx_insert_counter_update(drcontext, bb, instr, spill_slot_max,
                                          &stats.num_fcmps[i], per_bb->num_fcmps[i], flags);
	    }
	}
    }

#if defined(VERBOSE) && defined(VERBOSE_VERBOSE)
    dr_printf("Finished instrumenting dynamorio_basic_block(tag=" PFX ")\n", tag);
    instrlist_disassemble(drcontext, tag, bb, STDOUT);
#endif
    if (drmgr_is_last_instr(drcontext, instr)) 
	    dr_thread_free(drcontext, per_bb, sizeof(*per_bb));
    return DR_EMIT_DEFAULT;
}

DR_EXPORT void
dr_client_main(client_id_t id, int argc, const char *argv[])
{
    drreg_options_t ops = { sizeof(ops), 1 /*max slots needed: aflags*/, false };
    dr_set_client_name("DynamoRIO Sample Client 'exec_stats'",
                       "http://dynamorio.org/issues");

    /* Options */
    if (!droption_parser_t::parse_argv(DROPTION_SCOPE_CLIENT, argc, argv, NULL, NULL)) {
        dr_printf(droption_parser_t::usage_short(DROPTION_SCOPE_CLIENT).c_str());
	exit(1);
    }

    /* Get main module address */
    if (!all_modules.get_value()) {
        module_data_t *exe = dr_get_main_module();
        if (exe != NULL)
            exe_start = exe->start;
        dr_free_module_data(exe);
    }

    if (!drmgr_init() || !drx_init() || drreg_init(&ops) != DRREG_SUCCESS)
        DR_ASSERT(false);
    client_id = id;

    /* register events */
    dr_register_exit_event(event_exit);
    if (!drmgr_register_bb_instrumentation_event(event_bb_analysis, event_app_instruction, NULL))
        DR_ASSERT(false);

    /* make it easy to tell, by looking at log file, which client executed */
    dr_log(NULL, DR_LOG_ALL, 1, "Client 'exec_stats' initializing\n");
#ifdef SHOW_RESULTS
    /* also give notification to stderr */
    if (dr_is_notify_on()) {
#    ifdef WINDOWS
        /* ask for best-effort printing to cmd window.  must be called at init. */
        dr_enable_console_printing();
#    endif
        dr_fprintf(STDERR, "Client exec_stats is running\n");
    }
#endif
}
