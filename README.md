# dr-exec-stats

A simple Dynamorio execution stats client library

Combines several of the sample clients to record execution stats:
- \# of instructions executed
- \# of floating point intructions executed
- \# of basic blocks executed
- \# of normal comparison instructions
- \# of floating point comparison instructions
- \# of predicate instructions
- \# of syscalls
- \# of comparsion instructions by operand size

# Usage

```shell
echo "1, 3, 6, 1.3, 7.6" | /opt/dr/bin64/drrun -c ../build/bin/libexec_stats.so -log_dir /tmp/ex -- ./loop
Client exec_stats is running
Enter the values < d, d, d, f, f > :
Results:        16      79.039993       100
{
 "exec_stats": {
    "instructions": 291,
    "fp_instructions": 19,
    "basic_blocks": 54,
    "syscalls": 1,
    "pred_instrs": 0,
    "cmp_instrs": 12,
    "fcmp_instrs": 6
    },
 "cmp_by_size": [0, 0, 2, 0, 0, 0, 10, 0, 0, 0, 0],
 "fcmp_by_size": [0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0]
}
 --- wrote results to /tmp/ex/dr_exec_stats.loop.14388.0000.log (290 bytes)
```

## Options for `libexec_stats.so`:
```shell
 -all                 [ false]  Count all instructions (app +lib)
 -quiet               [ false]  Quiet (disable stdout logging)
 -log_dir             [  /tmp]  logs are written to log_dir
```

# How to build

```shell
$ mkdir build && cd build
$ cmake -DDynamoRIO_DIR=</path/to/dr/cmake> ..
$ make
```


