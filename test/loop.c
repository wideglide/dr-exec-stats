#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <asm/unistd.h>

static int *bug = NULL;

float mul(float a,  double b) {
	while (a < b) {
		a += a;
	}

	if (a == b) {
		fprintf(stderr, " ERROR: %g == %g \n", a, b);
		fflush(stderr);
		*bug += 1;
	}
	return a * b;
}

int add(int a, int b, long long c) {
	for (int i=0; i < c; i++) {
		b += a;
	}
	return a + b + c;
}

int sub(char a, char b) {
	while (a > b) {
		a -= 2;
	}
	return (int) a+b;
}

void take_input() {
	char c1, c2, buf[512];
	int r, x, y, z;
	float f1, f2;

	printf("Enter the values < d, d, d, f, f > : ");
#ifdef __i386__
#define __NR_read 3
	asm volatile
	(
	 "int $0x80"
	 : "=r"(r)
	 : "0"(__NR_read), "b"(0), "c"(buf), "d"(511)
	 : "memory"
	);
#endif /* __i386__ */

#ifdef __x86_64__
#define __NR_read 0
	asm volatile
	(
	 "syscall"
	 : "=a"(r)
	 : "0"(__NR_read), "D"(0), "S"(buf), "d"(511)
	 : "rcx", "r11", "memory"
	);
#endif /* __x86_64__ */
		
	sscanf(buf, "%d, %d, %d, %f, %f", &x, &y, &z, &f1, &f2);

	c1 = (char) x + 0x30 % 0x40;
	c2 = (char) y + 0x30 % 0x40;

	printf("\nResults:\t%d\t%f\t%d\n", add(x, y, z), mul(f1, f2), sub(c1, c2));
}

int main() {
	take_input();
}
